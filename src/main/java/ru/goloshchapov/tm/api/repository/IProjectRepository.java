package ru.goloshchapov.tm.api.repository;

import ru.goloshchapov.tm.api.IBusinessRepository;
import ru.goloshchapov.tm.model.Project;

public interface IProjectRepository extends IBusinessRepository<Project> {


}
