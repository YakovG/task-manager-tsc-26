package ru.goloshchapov.tm.exception;

import org.jetbrains.annotations.Nullable;

public abstract class AbstractException extends RuntimeException {

    public AbstractException() {
    }

    public AbstractException(@Nullable final String message) {
        super(message);
    }

    public AbstractException(@Nullable final String message, @Nullable final Throwable cause) {
        super(message, cause);
    }

    public AbstractException(@Nullable final Throwable cause) {
        super(cause);
    }

    public AbstractException(
            @Nullable final String message, @Nullable final Throwable cause,
            boolean enableSuppression, boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
