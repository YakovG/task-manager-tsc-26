package ru.goloshchapov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.goloshchapov.tm.api.repository.ITaskRepository;
import ru.goloshchapov.tm.api.service.ITaskService;
import ru.goloshchapov.tm.exception.empty.EmptyNameException;
import ru.goloshchapov.tm.model.Task;


import static ru.goloshchapov.tm.util.ValidationUtil.*;

public final class TaskService extends AbstractBusinessService<Task> implements ITaskService {

    @NotNull private final ITaskRepository taskRepository;

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(userId, task);
        return task;
    }

}
